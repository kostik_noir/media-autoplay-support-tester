/* eslint-disable no-console */

const audio = document.querySelector('audio');

document.addEventListener('click', onUserInteraction, false);
document.addEventListener('keypress', onUserInteraction, false);

onStartup();

function onStartup() {
  console.log('>>> on startup');
  test();
  audio.play();
}

function onUserInteraction() {
  console.log('>>> after user interaction');
  test();
  audio.play();
}

function test() {
  window.ml.utils.mediaAutoplaySupportTester.test(function(result) {
    if (result) {
      console.log('autoplay is allowed');
    } else {
      console.log('autoplay is not allowed');
    }
  });
}
