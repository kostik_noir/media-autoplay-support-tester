import mp3File from './assets/test-sound.mp3';
import wavFile from './assets/test-sound.wav';

export function attachToGlobal(obj) {
  const namespaceParts = [
    'ml',
    'utils'
  ];

  let parent = window;
  namespaceParts.forEach((name) => {
    if (typeof parent[name] === 'undefined') {
      parent[name] = {};
    }
    parent = parent[name];
  });

  parent.mediaAutoplaySupportTester = obj;
}

export function createAudio() {
  const audio = document.createElement('audio');
  switch (true) {
    case (audio.canPlayType('audio/mpeg') !== ''):
      audio.setAttribute('src', mp3File);
      break;
    case (audio.canPlayType('audio/wav') !== ''):
      audio.setAttribute('src', wavFile);
      break;
  }
  audio.style.display = 'block';
  audio.style.width = '1px';
  audio.style.height = '1px';
  audio.style.position = 'absolute';
  audio.style.left = '-1000000000px';
  audio.style.top = '-1000000000px';
  return audio;
}
