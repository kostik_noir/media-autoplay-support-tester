import * as utils from './utils';

utils.attachToGlobal({ test: test });

let audio;
let isTestInProgress = false;
let listeners = [];

export function test(cb) {
  listeners.push(cb);

  if (isTestInProgress) {
    return;
  }
  isTestInProgress = true;

  runTest(onTestCompleted);

  function onTestCompleted(result) {
    isTestInProgress = false;
    notifyListeners(result);
  }

  function notifyListeners(result) {
    listeners.forEach(fn => fn(result));
    listeners = [];
  }
}

function runTest(cb) {
  audio = utils.createAudio();
  document.body.appendChild(audio);

  let timeoutId = Number.NaN;
  const playPromise = audio.play();

  if (typeof playPromise !== 'undefined') {
    playPromise
      .then(onPlaybackStarted)
      .catch(onPlaybackCanceled);
    return;
  }

  audio.addEventListener('play', onPlaybackStarted, false);
  timeoutId = setTimeout(onPlaybackCanceled, 100);

  function onPlaybackStarted() {
    disposeAudio();
    clearTimeout(timeoutId);
    onAutoPlayEnabled();
  }

  function onPlaybackCanceled() {
    disposeAudio();
    clearTimeout(timeoutId);
    onAutoPlayDisabled();
  }

  function disposeAudio() {
    audio.removeEventListener('play', onPlaybackStarted, false);
    document.body.removeChild(audio);
    audio = null;
  }

  function onAutoPlayEnabled() {
    cb(true);
  }

  function onAutoPlayDisabled() {
    cb(false);
  }
}
