const { isProductionMode } = require('../../project-config');

module.exports = (webpackCfg) => {
  webpackCfg.output.pathinfo = !isProductionMode;

  webpackCfg.performance.hints = false;

  webpackCfg.devtool = isProductionMode ? false : 'source-map';
};
