const webpack = require('webpack');
const TerserPlugin = require('terser-webpack-plugin');
const { isProductionMode } = require('../../project-config');

module.exports = (webpackCfg) => {
  webpackCfg.module.rules.push({
    test: /\.js$/,
    use: [
      {
        loader: 'babel-loader'
      }
    ],
    exclude: [/node_modules/]
  });

  webpackCfg.module.noParse.push(/\.min\.js/);

  if (isProductionMode) {
    webpackCfg.optimization.minimizer.push(new TerserPlugin());

    webpackCfg.plugins.push(new webpack.optimize.MinChunkSizePlugin({
      minChunkSize: 51200
    }));
  }
};
