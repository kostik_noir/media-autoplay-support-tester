const path = require('path');
const { sourceRootDir } = require('../../project-config');

module.exports = (webpackConfig) => {
  webpackConfig.entry = {
    'media-autoplay-support-tester': path.resolve(sourceRootDir, 'media-autoplay-support-tester/index.js'),
    demo: path.resolve(sourceRootDir, 'demo/index.js')
  };
};
