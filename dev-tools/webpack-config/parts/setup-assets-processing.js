const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
const { sourceRootDir, distRootDir, isProductionMode } = require('../../project-config');

const assetNamePattern = 'assets/[name]_[hash].[ext]';

const imagesRule = {
  test: /\.(gif|png|jpe?g|ico)$/,
  use: [
    {
      loader: 'url-loader',
      options: {
        limit: 1000,
        name: assetNamePattern
      }
    }
  ]
};
if (isProductionMode) {
  imagesRule.use.unshift({
    loader: 'image-webpack-loader'
  });
}

const fontsRule = {
  test: /\.(eot|svg|ttf|woff|woff2)$/,
  use: [
    {
      loader: 'file-loader',
      options: {
        name: assetNamePattern
      }
    }
  ]
};

const audioFilesRule = {
  test: /\.(mp3|ogg|wav)$/,
  use: [
    {
      loader: 'url-loader'
    }
  ]
};

module.exports = (webpackCfg) => {
  webpackCfg.module.rules.push(imagesRule);
  webpackCfg.module.rules.push(fontsRule);
  webpackCfg.module.rules.push(audioFilesRule);

  if (isProductionMode) {
    webpackCfg.plugins.push(new CopyPlugin([
      {
        from: path.join(sourceRootDir, 'demo/*.+(mp3|ogg)'),
        to: path.join(distRootDir, 'demo/'),
        flatten: true
      }
    ]));
  }
};
