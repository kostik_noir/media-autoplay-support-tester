# Dev

+ `yarn dist` - build release version of the application
+ `yarn dev` - run dev server and build dev version of the application

# Test sound files in `src/media-autoplay-support-tester/assets`

## MP3 file

`ffmpeg -y -ar 11025 -t 0.01 -f s16le -codec:a pcm_s16le -ac 1 -i /dev/zero -codec:a libmp3lame -b:a 8k output.mp3`

## WAV file

`ffmpeg -y -ar 8000 -t 0.01 -f s16le -codec:a pcm_s16le -ac 1 -i /dev/zero -codec:a pcm_s16le output.wav`
